/* Copyright 2018 - 2020 - 2020 Martin Dosch

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License. */

package main

import (
	"encoding/json"
	"fmt"
	"log"
	"os"
	"os/user"
	"strings"
	//	"time"

	//	"github.com/chilts/sid"
	"gosrc.io/xmpp"
	"gosrc.io/xmpp/stanza"
)

// configuration is defined as global as it is needed by function
// messageProcessing.
type configuration struct {
	Address         string
	BotJid          string
	Password        string
	ControlJid      string
	Twtxtnick       string
	TimelineEntries int
	MaxCharacters   int
}

func main() {

	var err error
	var configpath string
	// Get systems user config path.
	osConfigDir := os.Getenv("$XDG_CONFIG_HOME")
	if osConfigDir != "" {
		// Create configpath if not yet existing.
		configpath = osConfigDir + "/.config/goxtxt/"
		if _, err := os.Stat(configpath + "config.json"); os.IsNotExist(err) {
			err = os.MkdirAll(configpath, 0700)
			if err != nil {
				log.Fatal("Error: ", err)
			}
		}
	} else { // Get the current user.
		curUser, err := user.Current()
		if err != nil {
			log.Fatal("Error: ", err)
			return
		}
		// Get home directory.
		home := curUser.HomeDir

		if home == "" {
			log.Fatal("Error: No home directory available.")
			return
		}

		// Create configpath if not yet existing.
		configpath = home + "/.config/goxtxt/"
		if _, err := os.Stat(configpath + "config.json"); os.IsNotExist(err) {
			err = os.MkdirAll(configpath, 0700)
			if err != nil {
				log.Fatal("Error: ", err)
			}
		}

	}

	// Check that config file is existing.
	if _, err := os.Stat(configpath + "config.json"); os.IsNotExist(err) {
		log.Fatal("Error: ", err)
	}

	// Read configuration file into variable config.
	file, _ := os.Open(configpath + "config.json")
	defer file.Close()
	decoder := json.NewDecoder(file)
	config := configuration{}
	if err := decoder.Decode(&config); err != nil {
		log.Fatal("Error: ", err)
	}

	// Set xmpp connection options according the config.
	options := xmpp.Config{
		TransportConfiguration: xmpp.TransportConfiguration{
			Address: config.Address,
		},
		Jid:        config.BotJid,
		Credential: xmpp.Password(config.Password),
		// StreamLogger: os.Stdout,
		Insecure: false}

	router := xmpp.NewRouter()
	router.NewRoute().
		Packet("message").
		HandlerFunc(func(s xmpp.Sender, p stanza.Packet) {
			handleMessage(s, p, config)
		})
	router.HandleFunc("presence", handlePresence)

	client, err := xmpp.NewClient(&options, router, errorHandler)
	if err != nil {
		log.Fatalf("%+v", err)
	}

	cm := xmpp.NewStreamManager(client, nil)
	log.Fatal(cm.Run())
}

func handleMessage(s xmpp.Sender, p stanza.Packet, config configuration) {
	msg, ok := p.(stanza.Message)
	if !ok {
		_, _ = fmt.Fprintf(os.Stdout, "Ignoring packet: %T\n", p)
		return
	}

	// Check if message comes from JID who is allowed to use this bot
	if !strings.HasPrefix(msg.From, config.ControlJid) {
		reply := stanza.Message{Attrs: stanza.Attrs{To: msg.From, Type: "chat"},
			Body: "You're not allowed to control me."}
		_ = s.Send(reply)
		return
	}
	// Process the message.
	processMessage(s, msg, config)
}

func handlePresence(s xmpp.Sender, p stanza.Packet) {
	pres, ok := p.(stanza.Presence)
	if !ok {
		_, _ = fmt.Fprintf(os.Stdout, "Ignoring packet: %T\n", p)
		return
	}
	switch pres.Type {
	case "subscribe":
		reply := stanza.Presence{Attrs: stanza.Attrs{To: pres.From, Type: "subscribed"}}
		_ = s.Send(reply)
		reply = stanza.Presence{Attrs: stanza.Attrs{To: pres.From, Type: "subscribe"}}
		_ = s.Send(reply)
	}
}

func errorHandler(err error) {
	log.Println(err.Error())
}
