# github.com/google/uuid v1.1.5
github.com/google/uuid
# github.com/klauspost/compress v1.11.7
github.com/klauspost/compress/flate
# golang.org/x/xerrors v0.0.0-20200804184101-5ec99f83aff1
golang.org/x/xerrors
golang.org/x/xerrors/internal
# gosrc.io/xmpp v0.5.1
gosrc.io/xmpp
gosrc.io/xmpp/stanza
# nhooyr.io/websocket v1.8.6
nhooyr.io/websocket
nhooyr.io/websocket/internal/bpool
nhooyr.io/websocket/internal/errd
nhooyr.io/websocket/internal/wsjs
nhooyr.io/websocket/internal/xsync
