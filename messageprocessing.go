/* Copyright 2018 - 2020 - 2020 Martin Dosch

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License. */

package main

import (
	"strconv"
	"strings"

	"gosrc.io/xmpp"
	"gosrc.io/xmpp/stanza"
	"salsa.debian.org/mdosch/goxtxt/twtxt"
)

// processMessage is executing the twtxt commands according to messages
// received and replies the output.
func processMessage(s xmpp.Sender, msg stanza.Message, config configuration) {

	// Stop processing if the body is empty
	if msg.Body == "" {
		return
	}

	words := strings.Fields(msg.Body)
	// First word of message body contains the command.
	switch strings.ToLower(words[0]) {
	// Show help message.
	case "help":
		reply := stanza.Message{Attrs: stanza.Attrs{To: msg.From,
			Type: "chat"}, Body: "\"help\": Show this message.\n" +
			"\"ping\": Bot replies if available.\n" +
			"\"tl\": Show last " + strconv.Itoa(config.TimelineEntries) +
			" timeline entries.\n" +
			"\"tv [user]\": Show [user]s timeline.\n" +
			"\"tw [tweet]\": Will tweet your input [tweet].\n" +
			"\"tm [user]\": Will show the last " + strconv.Itoa(config.TimelineEntries) +
			" mentions. [user] will fall back  to \"" + config.Twtxtnick + "\" if not specified.\n" +
			"\"tt [tag]\": Will show the last " + strconv.Itoa(config.TimelineEntries) +
			" occurrences of #[tag]\n" +
			"\"tf [user] [url]\": Follow [user].\n" +
			"\"tu [user]\": Unfollow [user].\n" +
			"\"to\": List the accounts you are following.\n" +
			"\"source\": Shows a link to the sourcecode."}
		_ = s.Send(reply)
	// Reply to a ping request.
	case "ping":
		reply := stanza.Message{Attrs: stanza.Attrs{To: msg.From, Type: "chat"},
			Body: "Pong"}
		_ = s.Send(reply)
	// Show link to source code repository.
	case "source":
		reply := stanza.Message{Attrs: stanza.Attrs{To: msg.From, Type: "chat"},
			Body: "https://salsa.debian.org/mdosch/goxtxt/"}
		_ = s.Send(reply)
	// Send a tweet.
	case "tw":
		// Check that message body contains something to tweet.
		if len(words) == 1 {
			reply := stanza.Message{Attrs: stanza.Attrs{To: msg.From, Type: "chat"},
				Body: "No Input."}
			_ = s.Send(reply)
			break
		}
		// Check that tweet doesn't exceed configured maximum length.
		tweetLength := len(msg.Body) - 3
		if tweetLength > config.MaxCharacters {
			reply := stanza.Message{Attrs: stanza.Attrs{To: msg.From, Type: "chat"},
				Body: "Tweet exceeds maximum of " + strconv.Itoa(config.MaxCharacters) +
					" characters by " + strconv.Itoa(tweetLength-config.MaxCharacters) +
					" characters."}
			_ = s.Send(reply)
			break
		}
		// Send the tweet.
		_, err := twtxt.Tweet(words[1:])
		if err != nil {
			reply := stanza.Message{Attrs: stanza.Attrs{To: msg.From, Type: "chat"},
				Body: "Failed."}
			_ = s.Send(reply)
			break
		}
	// Show timeline.
	case "tl":
		out, err := twtxt.Timeline(config.TimelineEntries)
		if err != nil {
			reply := stanza.Message{Attrs: stanza.Attrs{To: msg.From, Type: "chat"},
				Body: "Failed."}
			_ = s.Send(reply)
			break
		}
		reply := stanza.Message{Attrs: stanza.Attrs{To: msg.From, Type: "chat"},
			Body: out}
		_ = s.Send(reply)
	// Show only timeline entries from a certain user.
	case "tv":
		// Check there is a username specified.
		if len(words) == 1 {
			reply := stanza.Message{Attrs: stanza.Attrs{To: msg.From, Type: "chat"},
				Body: "No Input."}
			_ = s.Send(reply)
			break
		}
		// Check there is not more than one username specified.
		if len(words) > 2 {
			reply := stanza.Message{Attrs: stanza.Attrs{To: msg.From, Type: "chat"},
				Body: "Timeline view supports only one user."}
			_ = s.Send(reply)
			break
		}
		out, err := twtxt.ViewUser(config.TimelineEntries, words[1])
		if err != nil {
			reply := stanza.Message{Attrs: stanza.Attrs{To: msg.From, Type: "chat"},
				Body: "Failed."}
			_ = s.Send(reply)
			break
		}
		reply := stanza.Message{Attrs: stanza.Attrs{To: msg.From, Type: "chat"},
			Body: out}
		_ = s.Send(reply)
	// Show @-mentions of a certain user.
	case "tm":
		// If no username specified show mentions of own user.
		if len(words) == 1 {
			out, err := twtxt.Mentions(config.Twtxtnick, config.TimelineEntries)
			if err != nil {
				reply := stanza.Message{Attrs: stanza.Attrs{To: msg.From, Type: "chat"},
					Body: "Failed."}
				_ = s.Send(reply)
				break
			}
			reply := stanza.Message{Attrs: stanza.Attrs{To: msg.From, Type: "chat"},
				Body: out}
			_ = s.Send(reply)
		}
		// Show mentions of the specified user.
		if len(words) == 2 {
			out, err := twtxt.Mentions(words[1], config.TimelineEntries)
			if err != nil {
				reply := stanza.Message{Attrs: stanza.Attrs{To: msg.From, Type: "chat"},
					Body: "Failed."}
				_ = s.Send(reply)
				break
			}
			reply := stanza.Message{Attrs: stanza.Attrs{To: msg.From, Type: "chat"},
				Body: out}
			_ = s.Send(reply)
		}
		// Check that there is not more than one user specified.
		if len(words) > 2 {
			reply := stanza.Message{Attrs: stanza.Attrs{To: msg.From, Type: "chat"},
				Body: "Too many arguments."}
			_ = s.Send(reply)
		}
	// Show timeline entries containing a certain #-tag.
	case "tt":
		// Check that a tag is specified.
		if len(words) == 1 {
			reply := stanza.Message{Attrs: stanza.Attrs{To: msg.From, Type: "chat"},
				Body: "Missing Input."}
			_ = s.Send(reply)
		}
		// Show timeline entries for the specified tag.
		if len(words) == 2 {
			out, err := twtxt.Tags(strings.ToLower(words[1]), config.TimelineEntries)
			if err != nil {
				reply := stanza.Message{Attrs: stanza.Attrs{To: msg.From, Type: "chat"},
					Body: "Failed."}
				_ = s.Send(reply)
				break
			}
			reply := stanza.Message{Attrs: stanza.Attrs{To: msg.From, Type: "chat"},
				Body: out}
			_ = s.Send(reply)
		}
		// Check that there is only one tag specified.
		if len(words) > 2 {
			reply := stanza.Message{Attrs: stanza.Attrs{To: msg.From, Type: "chat"},
				Body: "Too many arguments."}
			_ = s.Send(reply)
		}
	// Add a certain user to follow.
	case "tf":
		// Check that username and URL are specified.
		if len(words) != 3 {
			reply := stanza.Message{Attrs: stanza.Attrs{To: msg.From, Type: "chat"},
				Body: "Missing Input."}
			_ = s.Send(reply)
			break
		}
		// Follow the specified user.
		out, err := twtxt.UserManagement(true, words[1:])
		if err != nil {
			reply := stanza.Message{Attrs: stanza.Attrs{To: msg.From, Type: "chat"},
				Body: "Failed."}
			_ = s.Send(reply)
			break
		}
		reply := stanza.Message{Attrs: stanza.Attrs{To: msg.From, Type: "chat"},
			Body: out}
		_ = s.Send(reply)
	// Stop following a certain user.
	case "tu":
		// Check that only one username is specified.
		if len(words) != 2 {
			reply := stanza.Message{Attrs: stanza.Attrs{To: msg.From, Type: "chat"},
				Body: "Wrong parameter count."}
			_ = s.Send(reply)
			break
		}
		// Unfollow the specified user.
		out, err := twtxt.UserManagement(false, words[1:])
		if err != nil {
			reply := stanza.Message{Attrs: stanza.Attrs{To: msg.From, Type: "chat"},
				Body: "Failed."}
			_ = s.Send(reply)
			break
		}
		reply := stanza.Message{Attrs: stanza.Attrs{To: msg.From, Type: "chat"},
			Body: out}
		_ = s.Send(reply)
	// Retrieve a list of users we are following.
	case "to":
		out, err := twtxt.ListFollowing()
		if err != nil {
			reply := stanza.Message{Attrs: stanza.Attrs{To: msg.From, Type: "chat"},
				Body: "Failed."}
			_ = s.Send(reply)
			break
		}
		reply := stanza.Message{Attrs: stanza.Attrs{To: msg.From, Type: "chat"},
			Body: out}
		_ = s.Send(reply)
	// Point help command if an unknown command is received.
	default:
		reply := stanza.Message{Attrs: stanza.Attrs{To: msg.From, Type: "chat"},
			Body: "Unknown command. Send \"help\"."}
		_ = s.Send(reply)
	}
}
